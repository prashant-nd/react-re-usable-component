import React from 'react';
import OnclickTooltip from './OnclickTooltip';
import OnhoverTooltip from './OnhoverTooltip';
import TextTruncation from './TextTruncation';

const App = () => {
  return (
    <div className="App mt-5">
      <OnclickTooltip />
      <hr />
      <OnhoverTooltip />
      <hr />
      <TextTruncation />
    </div>
  );
};

export default App;
