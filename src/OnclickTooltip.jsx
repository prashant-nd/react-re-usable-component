import React, { useState, useRef } from 'react';
import { Button, Tooltip, Overlay } from 'react-bootstrap';

const OnclickTooltip = () => {
  // use State to show and hide tooltip //

  const [show, setShow] = useState(false);
  const target = useRef(null);
  return (
    <div>
      <h3>Clickable tooltip</h3>
      <Button ref={target} onClick={() => setShow(!show)}>
        Click me!
      </Button>

      {/* use placement to show tooltip sides */}
      <Overlay target={target.current} show={show} placement="right">
        {(props) => (
          <Tooltip id="overlay-example" {...props}>
            This is tooltip
          </Tooltip>
        )}
      </Overlay>
    </div>
  );
};

export default OnclickTooltip;
