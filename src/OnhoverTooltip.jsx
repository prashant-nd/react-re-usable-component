import React from 'react';
import { Button, Tooltip, OverlayTrigger } from 'react-bootstrap';

const OnhoverTooltip = () => {
    // Storing sides of tooltip //
  const side = ['top', 'bottom', 'right', 'left'];
  return (
    <div>
      <h3>Hover tooltip</h3>

        {/* map sides */}
      {side.map((side) => (
        //  wrap  button with OverlayTrigger //

        <OverlayTrigger
          placement={side}        // pass position in placement ex:'top','left' //
          overlay={<Tooltip>Tooltip {side}</Tooltip>}
        >
          <Button variant="danger" className="m-2">
            {side}
          </Button>
        </OverlayTrigger>
      ))}

      <hr /> 

      {/* Tooltip for Image */}
      <h3> Tooltip for Image</h3>
        {/* wrap  image with OverlayTrigger  */}
      <OverlayTrigger
        placement={'left'}
        overlay={<Tooltip>Image on hover left tooltip</Tooltip>}
      >

        <img
          src="https://www.freecodecamp.org/news/content/images/2021/06/Ekran-Resmi-2019-11-18-18.08.13.png"
          style={{ width: '20%' }}
          alt="No Image"
        ></img>
      </OverlayTrigger>
    </div>
  );
};

export default OnhoverTooltip;
