import React from 'react';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

const TextTruncation = () => {
  // store your content //
  const content = ' A B C D E F G H I G K L';
  const content2 =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
  return (
    <div id="text_truncate">
      <h3>Text truncation with tooltip</h3>
      {/* wrap  image with OverlayTrigger  */}
      <OverlayTrigger
        placement={'bottom'}
        overlay={
            
          <Tooltip className="nd-tooltip">
            {content2}
          </Tooltip>
        }
      >
        {/* use "text-truncate" className for text truncation with maxWidth */}
        <div className="d-inline-block text-truncate" style={{ maxWidth: 200 }}>
          {content2}
        </div>
      </OverlayTrigger>
    </div>
  );
};

export default TextTruncation;
